package main

import "fmt"

/*
  1) Iterate over the chars in the string and assign each char to a map.
  NOTE: Map Keys will be the string char and the values set to 0.

  2) Iterate again over the string and evaluate if the char exists in the map.

  3) If the char does exist increment the inital value of 0 by 1.
  NOTE: If the char is found again in the string once again the number stored for that key will be incremented by 1 again and so on.

  4) The work is done with the counts so now to display the results.  Iterate over each key and value in the map.  If the int stored with that key is greater than 1 that means it is a repeat.

  5) If the number is greater than one perform an iteration over that number and print the char in the map.

*/

func main() {
  fmt.Println("Print Duplicate Characters")
  data := "letters"

  charMap := make(map[string]int)
  for _,c := range data {
    fmt.Println(string(c))
    charMap[string(c)] = 0
      }
         fmt.Println("MAP: ",charMap)
    for _,c := range data {
     if _,ok := charMap[string(c)]; ok {
        charMap[string(c)]++
      }
    }
    fmt.Println("NEWMAP: ",charMap)
    for k,v := range charMap {
      if v > 1 {
        for i := 0; i < v; i++ {
          fmt.Printf("Found: %s\n",k)
        } 
      }
    }
    }