package main

import (
  "fmt"
  "math/rand"
  "time"
  "errors"
  "log"
  "sort"
)

/*
  1) Create the Random number function
  2) Create the Array to hold your numbers
  3) Call the function within a for iterator and fill your array
  4) Use the sort library func sort.Intn to sort the array in order
*/

func main() {
  fmt.Println("Sorting Numbers in Array")

  var numbers []int

  for i:=0; i < 10; i++ {
    n, err := randomInt(100)
    if err != nil {
      log.Println(err)
    }
      numbers = append(numbers,n)
    }
   sort.Ints(numbers)
    fmt.Println(numbers)
  }

// randomInt - generates a random integer using the n argument as the range for the generation
  func randomInt(n int) (int, error) {
    if n <= 0 {
      return 0, errors.New("number larger than 0 required")
    }
  s := rand.NewSource(time.Now().UnixNano())
  r := rand.New(s)

  return r.Intn(n), nil
  }